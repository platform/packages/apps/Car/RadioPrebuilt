This apk was built with the code released to aosp on May 13, 2024.
To fetch and build the code, please follow the steps described here:
https://source.android.com/docs/devices/automotive/unbundled_apps/integration
